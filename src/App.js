import React, { Component } from 'react';
import logo from './logo.svg';
import html2canvas from 'html2canvas';
import { createParticleAtPoint, updateParticles } from './particles/factory';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      color: ''
    }
  }

  componentDidMount = async () => {
    this.btn = document.querySelector('button');
    const container = document.querySelector('.container');
    this.ctx = await html2canvas(this.btn).then(canvas =>  canvas.getContext("2d"));
    console.log(this.ctx);
    this.createParticalCanvas();
    window.requestAnimationFrame(() => updateParticles(this.particleCtx));

  }

  createParticalCanvas = () => {
    const particleCanvas = document.createElement('canvas');
    const particleCtx = particleCanvas.getContext('2d');

    particleCanvas.width = window.innerWidth;
    particleCanvas.height = window.innerHeight;

    particleCanvas.style.position = "absolute";
    particleCanvas.style.top = "0";
    particleCanvas.style.left = "0";
    particleCanvas.style.zIndex = "1001";
    particleCanvas.style.pointerEvents = "none";
    document.body.appendChild(particleCanvas);
    this.particleCanvas = particleCanvas;
    this.particleCtx = particleCtx;
  }

  getColorAtPoint = (e) => {
    const reductionFactor = 123;
    const width = this.btn.offsetWidth;
    const height = this.btn.offsetHeight;
    const colorData = this.ctx.getImageData(width, height, 1, 1).data;
    this.setState({ color: colorData.join(',') });

    let count = 0;

    for(let localX = 0; localX < width; localX++) {
      for(let localY = 0; localY < height; localY++) {
        if(count % reductionFactor === 0) {
          let index = (localY * width + localX) * 4;
          let rgbaColorArr = colorData.slice(index, index + 4);
          // Opposite color
          rgbaColorArr =   rgbaColorArr.map((color, i) => {
            if (i < 3) return 255 - color;
            return color;
          });

          let bcr = this.btn.getBoundingClientRect();
          let globalX = bcr.left + localX;
          let globalY = bcr.top + localY;

          createParticleAtPoint(globalX, globalY, rgbaColorArr);
        }
        count++;
      }
    }

    // const bcr = this.btn.getBoundingClientRect();
    // const globalX = bcr.left + x;
    // const globalY = bcr.top + y;
    // console.log(e.clientX, e.clientY);
    // console.log(x,  y);
    // console.log(globalX, globalY);
    // createParticleAtPoint(globalX, globalY, rbgaColorArr)
  }

  render() {
    return (
      <div className="container">
        <button onClick={this.getColorAtPoint}>Button</button>
        <div>
          <span>{`Color info (in RGBA format): ${this.state.color}`}</span>
        </div>
      </div>
    );
  }
}

export default App;
