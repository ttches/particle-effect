import ExplodingParticle from './explodingParticle';

let particles = [];

export const createParticleAtPoint = (x, y, colorData) => {
  const particle = new ExplodingParticle();
  particle.rgbArray = colorData;
  particle.startX = x;
  particle.startY = y;
  particle.startTime = Date.now();
  particles.push(particle);
}

export const updateParticles = (particleCtx) => {
  if(typeof particleCtx !== "undefined") {
    particleCtx.clearRect(0, 0, window.innerWidth, window.innerHeight);
  }
  particles.forEach((particle, i) => {
    particle.draw(particleCtx);

    if(i === particles.length - 1) {
      let percent = (Date.now() - particles[i].startTime) / particles[i].animationDuration;

      if(percent > 1) {
        particles = [];
      }
    }
  });
  window.requestAnimationFrame(() => updateParticles(particleCtx));
}